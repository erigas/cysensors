from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize

import sys

COMP_ARGS = [
    "-fopenmp",
    "-m64",
    "-Ofast",
    "-fno-finite-math-only",
    "-flto",
    "-march=native",
    "-funroll-loops",
    # "-fprofile-generate",
    # "-fprofile-use",
]
ext_modules = []
if "clean" not in sys.argv:
    ext_modules = cythonize(Extension(
            "sensors.cysensors",
            ["sensors/cysensors.pyx"],
            extra_compile_args=COMP_ARGS,
            extra_link_args=COMP_ARGS,
            libraries=["m", "sensors"],
    ))

setup(
    name='libsensors-cy',
    ext_modules=ext_modules,
    zip_safe=False,
)
