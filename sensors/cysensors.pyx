# cython: boundscheck=False
# cython: cdivision=True
# cython: wraparound=False
# cython: language_level=3
# cython: initializedcheck=False

from libc.string cimport memcpy

SENSOR_BUS_TYPE = {
    -1:  "ANY",
     1:  "I2C",
     2:  "ISA",
     3:  "PCI",
     4:  "SPI",
     5:  "VIRTUAL",
     6:  "ACPI",
     7:  "HID",
     8:  "MDIO",
     9:  "SCSI",
}

SENSOR_FEATURE_TYPE = {
    sensors_feature_type.SENSORS_FEATURE_IN:  "IN",
    sensors_feature_type.SENSORS_FEATURE_FAN:  "FAN",
    sensors_feature_type.SENSORS_FEATURE_TEMP:  "TEMP",
    sensors_feature_type.SENSORS_FEATURE_POWER:  "POWER",
    sensors_feature_type.SENSORS_FEATURE_ENERGY:  "ENERGY",
    sensors_feature_type.SENSORS_FEATURE_CURR:  "CURR",
    sensors_feature_type.SENSORS_FEATURE_HUMIDITY:  "HUMIDITY",
    sensors_feature_type.SENSORS_FEATURE_MAX_MAIN:  "MAX_MAIN",
    sensors_feature_type.SENSORS_FEATURE_VID:  "VID",
    sensors_feature_type.SENSORS_FEATURE_INTRUSION:  "INTRUSION",
    sensors_feature_type.SENSORS_FEATURE_MAX_OTHER:  "MAX_OTHER",
    sensors_feature_type.SENSORS_FEATURE_BEEP_ENABLE:  "BEEP_ENABLE",
    sensors_feature_type.SENSORS_FEATURE_MAX:  "MAX",
    sensors_feature_type.SENSORS_FEATURE_UNKNOWN:  "UNKNOWN",
}


SENSOR_SUBFEATURE_TYPE = {
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_INPUT: "IN_INPUT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_MIN: "IN_MIN",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_MAX: "IN_MAX",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_LCRIT: "IN_LCRIT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_CRIT: "IN_CRIT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_AVERAGE: "IN_AVERAGE",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_LOWEST: "IN_LOWEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_HIGHEST: "IN_HIGHEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_ALARM: "IN_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_MIN_ALARM: "IN_MIN_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_MAX_ALARM: "IN_MAX_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_BEEP: "IN_BEEP",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_LCRIT_ALARM: "IN_LCRIT_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_IN_CRIT_ALARM: "IN_CRIT_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_INPUT: "FAN_INPUT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_MIN: "FAN_MIN",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_MAX: "FAN_MAX",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_ALARM: "FAN_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_FAULT: "FAN_FAULT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_DIV: "FAN_DIV",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_BEEP: "FAN_BEEP",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_PULSES: "FAN_PULSES",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_MIN_ALARM: "FAN_MIN_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_FAN_MAX_ALARM: "FAN_MAX_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_INPUT: "TEMP_INPUT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_MAX: "TEMP_MAX",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_MAX_HYST: "TEMP_MAX_HYST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_MIN: "TEMP_MIN",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_CRIT: "TEMP_CRIT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_CRIT_HYST: "TEMP_CRIT_HYST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_LCRIT: "TEMP_LCRIT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_EMERGENCY: "TEMP_EMERGENCY",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_EMERGENCY_HYST: "TEMP_EMERGENCY_HYST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_LOWEST: "TEMP_LOWEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_HIGHEST: "TEMP_HIGHEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_MIN_HYST: "TEMP_MIN_HYST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_LCRIT_HYST: "TEMP_LCRIT_HYST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_ALARM: "TEMP_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_MAX_ALARM: "TEMP_MAX_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_MIN_ALARM: "TEMP_MIN_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_CRIT_ALARM: "TEMP_CRIT_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_FAULT: "TEMP_FAULT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_TYPE: "TEMP_TYPE",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_OFFSET: "TEMP_OFFSET",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_BEEP: "TEMP_BEEP",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_EMERGENCY_ALARM: "TEMP_EMERGENCY_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_TEMP_LCRIT_ALARM: "TEMP_LCRIT_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_AVERAGE: "POWER_AVERAGE",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_AVERAGE_HIGHEST: "POWER_AVERAGE_HIGHEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_AVERAGE_LOWEST: "POWER_AVERAGE_LOWEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_INPUT: "POWER_INPUT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_INPUT_HIGHEST: "POWER_INPUT_HIGHEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_INPUT_LOWEST: "POWER_INPUT_LOWEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_CAP: "POWER_CAP",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_CAP_HYST: "POWER_CAP_HYST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_MAX: "POWER_MAX",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_CRIT: "POWER_CRIT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_MIN: "POWER_MIN",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_LCRIT: "POWER_LCRIT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_AVERAGE_INTERVAL: "POWER_AVERAGE_INTERVAL",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_ALARM: "POWER_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_CAP_ALARM: "POWER_CAP_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_MAX_ALARM: "POWER_MAX_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_CRIT_ALARM: "POWER_CRIT_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_MIN_ALARM: "POWER_MIN_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_POWER_LCRIT_ALARM: "POWER_LCRIT_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_ENERGY_INPUT: "ENERGY_INPUT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_INPUT: "CURR_INPUT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_MIN: "CURR_MIN",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_MAX: "CURR_MAX",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_LCRIT: "CURR_LCRIT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_CRIT: "CURR_CRIT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_AVERAGE: "CURR_AVERAGE",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_LOWEST: "CURR_LOWEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_HIGHEST: "CURR_HIGHEST",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_ALARM: "CURR_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_MIN_ALARM: "CURR_MIN_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_MAX_ALARM: "CURR_MAX_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_BEEP: "CURR_BEEP",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_LCRIT_ALARM: "CURR_LCRIT_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_CURR_CRIT_ALARM: "CURR_CRIT_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_HUMIDITY_INPUT: "HUMIDITY_INPUT",
    sensors_subfeature_type.SENSORS_SUBFEATURE_VID: "VID",
    sensors_subfeature_type.SENSORS_SUBFEATURE_INTRUSION_ALARM: "INTRUSION_ALARM",
    sensors_subfeature_type.SENSORS_SUBFEATURE_INTRUSION_BEEP: "INTRUSION_BEEP",
    sensors_subfeature_type.SENSORS_SUBFEATURE_BEEP_ENABLE: "BEEP_ENABLE",
    sensors_subfeature_type.SENSORS_SUBFEATURE_UNKNOWN: "UNKNOWN",
}


#
# Abstraction Classes
#

cdef class Chip:
    cdef sensors_chip_name chip
    cdef list _features

    cdef new(self, sensors_chip_name *chip_name):
        self.chip.prefix = chip_name.prefix
        self.chip.addr = chip_name.addr
        self.chip.path = chip_name.path
        self.chip.bus = chip_name.bus
        self._features = self._get_features()
        return self

    @property
    def prefix(self):
       return self.chip.prefix.decode()

    @property
    def bus(self):
        return SENSOR_BUS_TYPE[self.chip.bus.type]

    @property
    def addr(self):
        return self.chip.addr

    @property
    def path(self):
        return self.chip.path.decode()

    @property
    def adapter(self):
        adapter = sensors_get_adapter_name(&self.chip.bus)
        if adapter == NULL:
            return None
        return adapter.decode()

    cdef _get_features(self):
        features = []
        for f in get_features(self):
            features.append(f)
        return features

    @property
    def features(self):
        return self._features

    def get_label(self, feature):
        return self._feature_label(feature).decode()

    cdef _feature_label(self, SensorsFeature feature):
        return sensors_get_label(&self.chip, feature._feature)

    def get_all_subfeatures(self, feature):
        return self._get_all_subfeatures(feature)

    cdef _get_all_subfeatures(self, SensorsFeature feature):
        return get_all_subfeatures(self, feature._feature)

    def get_value(self, num):
        return self._get_value(num)

    cdef double _get_value(self, int num):
        cdef double value = 0
        if sensors_get_value(&self.chip, num, &value) == 0:
            return value
        return 0.0

    def sensor_values(self):
        return self._sensors_get_values()

    cdef dict _sensors_get_values(self):
        cdef dict values = {}
        for feat in get_features(self):
            label = self.get_label(feat)
            values[label] = {}
            val = values[label]
            for sub in self._get_all_subfeatures(feat):
                val[sub.name] = self.get_value(sub.number)
        return values

    def __repr__(self):
       return "name: {}, path: {}, type: {}".format(self.chip.prefix,
                                                    self.chip.path,
                                                    SENSOR_BUS_TYPE[self.chip.bus.type])


cdef class SensorsFeature:
    cdef sensors_feature *_feature

    cdef new(self, sensors_feature *feature):
        self._feature =  feature
        return self

    @property
    def name(self):
       return self._feature.name.decode()

    @property
    def type(self):
        return SENSOR_FEATURE_TYPE[self._feature.type]

    @property
    def number(self):
        return self._feature.number

    def __repr__(self):
        return "Feature({},num:{},type: {})".format(self._feature.name,
                                                    self._feature.number,
                                                    SENSOR_FEATURE_TYPE[self._feature.type]
                                                    )


cdef class SensorsSubFeature:
    cdef sensors_subfeature *_feature

    cdef new(self, sensors_subfeature *feature):
        self._feature =  feature
        return self

    @property
    def name(self):
       return self._feature.name.decode()

    @property
    def type(self):
        return SENSOR_SUBFEATURE_TYPE[self._feature.type]

    @property
    def number(self):
        return self._feature.number

    def __repr__(self):
        return "SubFeature({},num:{},type: {})".format(self._feature.name,
                                                       self._feature.number,
                                                       SENSOR_SUBFEATURE_TYPE[self._feature.type]
                                                       )


cpdef init():
    return sensors_init(NULL)

cpdef cleanup():
    sensors_cleanup()

cpdef list get_detected_chips_prefix():
    name_list = []
    cdef int nr=0
    cdef sensors_chip_name *name

    while True:
        name = sensors_get_detected_chips(NULL, &nr)
        if name == NULL:
            break
        else:
            name_list.append(name.prefix.decode())
    return name_list

def get_detected_chips_iter():
    return DetectedChipsIter()

cdef class DetectedChipsIter:
    cdef int nr
    cdef sensors_chip_name *name

    def __iter__(self):
        self.nr = 0
        return self

    def __next__(self):
        return self._next()

    cdef object _next(self):
        self.name = sensors_get_detected_chips(NULL, &self.nr)
        if self.name == NULL:
            raise StopIteration
        chip = Chip().new(self.name)
        return chip


def get_features(Chip chip_name):
    feats = []
    cdef int nr = 0
    cdef sensors_feature *feat
    while True:
        feat = sensors_get_features(&chip_name.chip, &nr)
        if feat == NULL:
            break
        else:
            feats.append(SensorsFeature().new(feat))
    return feats

cdef list get_all_subfeatures(Chip chip, sensors_feature *feature):
    subfeats = []
    cdef int nr = 0
    cdef sensors_subfeature *subfeat
    while True:
        subfeat = sensors_get_all_subfeatures(&chip.chip, feature, &nr)
        if subfeat == NULL:
            break
        else:
            subfeats.append(SensorsSubFeature().new(subfeat))
    return subfeats