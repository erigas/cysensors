cysensors
=========

`cysensors` is a cython wrapper for the `libsensors` library from `lm-sensors`.

To build the module you will need `libsensors4-dev`.

```bash
python3 setup.py build_ext --inplace

```

The module is named `sensors` and to use it in python you do:
```python
import sensors

# Initialise library
sensors.init()

# print the detected chip prefixes
print("Detected the following chips:")
for i in sensors.get_detected_chips_prefix():
    print(f"\t- {i}")
print("\n")

# get_detected_chips_iter() returns an iterator with
# detected Chip instances
for c in sensors.get_detected_chips_iter():
    print(f"{c.prefix} - {c.adapter}")
    # the available features of each chip can be accessed
    # using the features attribute which returns a list of
    # SensorFeature objects
    for feature in c.features:
        print(f"\t{c.get_label(feature)} ({feature.name}):")
	# The subfeatures are accessed using get_all_subfeatures by
	# supplying the SensoreFeature object
	for d in c.get_all_subfeatures(feature):
            print(f"\t\t{d.name} ({d.type}): {c.get_value(d.number)}")
    print("\n")


# Cleanup deallocates the objects
sensors.cleanup()

```
