#!/usr/bin/python3
import sensors
from pprint import pprint
sensors.init()


print("Detected the following chips:")
for i in sensors.get_detected_chips_prefix():
    print(f"\t- {i}")
print("\n")

for c in sensors.get_detected_chips_iter():
    print(f"{c.prefix} - {c.adapter}")
    for feature in c.features:
        print(f"\t{c.get_label(feature)} ({feature.name}):")
        for d in c.get_all_subfeatures(feature):
            print(f"\t\t{d.name} ({d.type}): {c.get_value(d.number)}")
    print("\n")
    #pprint(c.sensor_values())


print("\nCORE TEMP")
chips = sensors.get_detected_chips_prefix()

coretemp_ind = None
if "coretemp" in chips:
    coretemp_ind = chips.index("coretemp")

if coretemp_ind > -1:
    chips = list(sensors.get_detected_chips_iter())
    coretemp = chips[coretemp_ind]
    for feat in coretemp.features:
        lbl = coretemp.get_label(feat)
        if "Core" not in lbl:
            continue
        inp, high, crit = 0., 0., 0.
        for sub in coretemp.get_all_subfeatures(feat):
            if sub.type == "TEMP_INPUT":
                inp = coretemp.get_value(sub.number)
            elif sub.type == "TEMP_MAX":
                high = coretemp.get_value(sub.number)
            elif sub.type == "TEMP_CRIT":
                crit = coretemp.get_value(sub.number)
        print(f"\t{lbl}: {inp} (high: {high}, crit: {crit})")

sensors.cleanup()
